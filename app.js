var mysql = require('mysql');
var config = require('./my-modules/config');
var express = require('express');
var request = require('request');
var app = express();

app.configure(function() {
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(express.static(__dirname + '/public'));
});

app.get('/', function(req, res){
  // res.send('Hello World');
  res.render('hello');
});

app.get('/artists/search', function(req, res) {
	res.render('artist-search');
});

app.get('/artists', function(req, res) {
	var connection = mysql.createConnection(config);

	var sql = "SELECT * FROM artists WHERE artist_name LIKE ?";
	var itunesUrl = 'http://itunes.apple.com/search?limit=25&entity=album&term=' + req.query.artist;
	
	// 1. Create a module called itunes with a method called getAlbums()
	// 2. Create a module called artist with a method called find() 
	// 3. Modify below to use callbacks, async.js, and lastly promises. Create
	// 		a separate branch for each one
	connection.query(sql, ['%' + req.query.artist + '%'], function(err, rows) {
		request({ url: itunesUrl, json: true }, function (error, response, body) {
		  if (!error && response.statusCode == 200) {
		    res.render('artists', {
					artists: rows,
					searchTerm: req.query.artist,
					albums: body.results
				});
		  }
		});
	});
	
});

app.listen(3000);