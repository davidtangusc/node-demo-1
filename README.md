In-class exercises
==================

It might be helpful to create a separate branch for each one.

#### 1. Create separate modules for the database and itunes api functionality

1. Create a module called itunes with a method called getAlbums(artistName)
2. Create a module called artist with a method called find(artistName) 


#### 2. Flow control with callbacks

Modify the /artists route to use callbacks with your newly created modules.

#### 3. Flow control with async.js

Now modify the /artists route to use async.js to manage flow control.

#### 4. Flow control with promises

Lastly, using the q package, have your 2 modules return promises. In your route, using q.all to listen for when both promises are resolved so you can render the view.


